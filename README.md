
# ZWallet
[![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](https://github.com/IndraPratama98/ZWallet)

# Description

This e-wallet that can transfer and receive balance from another account. Here is some preview of the application.

## SignUp
![](Images/SignUp.png)
## Login
![](Images/Login.png)
## Features
![](Images/Features.png)

# Table of Contents 

* [Installation](#installation)

* [Usage](#usage)

* [License](#license)

* [Contributing](#contributing)

* [Tests](#tests)

* [Questions](#questions)

## Installation
The following necessary dependencies must be installed to run the application properly: Cocoapods, Moya, Netfox, Kingfisher, OTPFieldView, NVAActivity

```js
platform :ios, '11.0'
use_frameworks!  
workspace 'ZWallet'
```

``` js
pod 'Moya'
```

```js
pod 'netfox', :configurations => ['Debug']
```

```js
def common_dependency
    pod 'Kingfisher'
end
```

```js
pod 'OTPFieldView'
```

```js
pod 'NVActivityIndicatorView'
```




## Usage
### Instructions
- ​This application is used for to do transaction and receive balance between account.
- You need to download this file, unzip it and go to the App folder. 
- Open the terminal and install the dependencies using
```
pod install
```
- Open ZWallet.xcworkspace
- Set your simulator and follow the instruction in this Readme Description before
- Congratulations now you can access all the Features

### Design Pattern
We are using Viper Design Pattern, consist of
- View
- Interactor
- Presenter
- Router

### VIPER Protocol
The generic issues about implementing design pattern are, when we want to have a simillar interface for every module, thats why i created a simple protocol for every following: 

#### View
Class that has all the code to show the app interface to the user and get their responses. Upon receiving a response View alerts the Presenter.
View consist of
1. ViewProtocol
2. ViewProtocolImpl 

#### Interactor
Has the business logic of an app. e.g if business logic depends on making network calls then it is Interactor’s responsibility to do so. We also created a simillar protocol but only for interactor that will be implemented in presenter in order to determine where the presenter will send the action.

Interactor consist of
1. InteractorProtocol
2. InteractorProtocolImpl 
3. InteractorOutput

#### Presenter
Engine of an module. It gets user response from the View and works accordingly. The only class to communicate with all the other components. Calls the router for wire-framing, Interactor to fetch data (network calls or local data calls), view to update the UI.

Presenter consist of
1. PresenterProtocol
2. PresenterProtocolImpl 

#### Entity
Contains plain model classes used by the Interactor.

#### Router
Does the wire-framing. Listens from the presenter about which screen to present and executes that.

Router consist of
1. RouterProtocol
2. RouterProtocolImpl 



## License

This project is license under the MIT license.

## Contributing

​Contributors: Indra Anugrah Pratama

## Questions

If you have any questions about the repo, open an issue or contact IndraPratama98 directly indra.pratama3224@gmail.com

