//
//  AppRouter.swift
//  Core
//
//  Created by MacBook on 24/05/21.
//

import Foundation
import UIKit


public class AppRouter {
    
    public static let shared: AppRouter = AppRouter()
    
    public var loginScene: (() -> ())? = nil
    
    public func navigateToLogin() {
        self.loginScene?()
    }
    
    public var homeScene: (() -> ())? = nil
    
    public func navigateToHome() {
        self.homeScene?()
    }
    
    public var historyScene: ((_ viewController: UIViewController) -> ())? = nil
    
    public func navigateToHistory(_ viewController: UIViewController) {
        self.historyScene?(viewController)
    }
    
    public var signUpScene: (() -> ())? = nil
    
    public func navigateToSignUp() {
        self.signUpScene?()
    }
    
    public var setOtpScene: ((_ email: String) -> ())? = nil
    
    public func navigateToSetOtp(_ email: String) {
        self.setOtpScene?(email)
    }
    
    public var createPinScene: (() -> ())? = nil
    
    public func navigateCreatePin() {
        self.createPinScene?()
    }
    
    public var showContactScene: ((_ viewController: UIViewController) -> ())? = nil
    
    public func navigateShowContact(_ viewController: UIViewController) {
        self.showContactScene?(viewController)
    }
    
    public var topUpScene: ((_ viewController: UIViewController) -> ())? = nil
    
    public func navigateTopUpScene(_ viewController: UIViewController) {
        self.topUpScene?(viewController)
    }
    
    public var transactionScene: ((_ viewController: UIViewController, _ receiverData: ProfileEntity) -> ())? = nil
    
    public func navigateToTransaction(_ viewController: UIViewController, _ receiverData: ProfileEntity) {
        self.transactionScene?(viewController, receiverData)
    }
    
    public var transactionContinueScene: ((_ viewController: UIViewController, _ receiverData: TransferEntity) -> ())? = nil
    
    public func navigateToTransactionContinue(_ viewController: UIViewController, _ transferData: TransferEntity) {
        self.transactionContinueScene?(viewController, transferData)
    }
    
    public var checkPinScene: ((_ viewController: UIViewController, _ receiverData: TransferEntity) -> ())? = nil
    
    public func navigateToCheckPin(_ viewController: UIViewController, _ transferData: TransferEntity) {
        self.checkPinScene?(viewController, transferData)
    }
    
    public var transactionResultScene: ((_ viewController: UIViewController, _ receiverData: TransferEntity, _ status: Bool) -> ())? = nil
    
    public func navigateTransactionResult(_ viewController: UIViewController, _ transferData: TransferEntity, _ status: Bool) {
        self.transactionResultScene?(viewController, transferData, status)
    }
    
    
}
