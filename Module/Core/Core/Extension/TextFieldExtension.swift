//
//  TextFieldExtension.swift
//  Core
//
//  Created by MacBook on 30/05/21.
//

import Foundation
import UIKit

public extension UITextField {
    func addBottomBorder(){
        let border = CALayer()
        let borderWidth = CGFloat(1.0)
        border.borderColor = #colorLiteral(red: 0.3807878494, green: 0.4607550502, blue: 0.9897784591, alpha: 1)
        border.frame = CGRect(origin: CGPoint(x: 0,y :self.frame.size.height - borderWidth), size: CGSize(width: self.frame.size.width, height: self.frame.size.height))
        border.borderWidth = borderWidth
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }

    
}

