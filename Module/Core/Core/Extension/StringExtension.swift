//
//  StringExtension.swift
//  Core
//
//  Created by MacBook on 28/05/21.
//

import Foundation

public extension String {
    func dateFormat() -> String {
        let dateFormatter = DateFormatter()
        let todaysDate = NSDate()
        dateFormatter.locale = Locale(identifier: "id")
        dateFormatter.dateFormat = self
        let date =  dateFormatter.string(from: todaysDate as Date)
        return date
    }
}
