//
//  AuthNetworkManagerImpl.swift
//  Core
//
//  Created by MacBook on 24/05/21.
//

import Foundation
import Moya

public class AuthNetworkmanagerImpl: AuthNetworkManager {
    public func transactionConfirm(receiver: Int, amount: Int, notes: String, pin: String, completion: @escaping (PinConfirmationResponse?, Error?) -> ()) {
        let provider = MoyaProvider<AuthApi>()
        provider.request(.transfer(receiver: receiver, amount: amount, notes: notes, pin: pin)) { response in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let pinConfirmationResponse = try decoder.decode(PinConfirmationResponse.self, from: result.data)
                    completion(pinConfirmationResponse, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil,error)
            }
            
            
        }
        
    }
    
    public func refreshToken(email: String, refreshToken: String, completion: @escaping (RefreshTokenDataResponse?, Error?) -> ()) {
        let provider = MoyaProvider<AuthApi> ()
        provider.request(.refreshToken(email: email, refreshToken: refreshToken)) { response in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let refreshTokenResponse = try decoder.decode(RefreshTokenResponse.self, from: result.data) as RefreshTokenResponse
                    completion(refreshTokenResponse.data, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil,error)
                
            }
            
            
            
        }
        
    }
    
  
    public init() {}
    
    public func login(email: String, password: String, completion: @escaping (LoginResponse?, Error?) -> ()) {
        let provider = MoyaProvider<AuthApi> ()
        provider.request(.login(email: email, password: password)) { response in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let loginResponse = try decoder.decode(LoginResponse.self, from: result.data)
                    completion(loginResponse, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil,error)
            }
        }
        
        
    }
    
    public func signUp(username: String, email: String, password: String, completion: @escaping (SignUpResponse?, Error?) -> ()) {
        let provider = MoyaProvider<AuthApi> ()
        provider.request(.signup(username: username, email: email, password: password)) { response in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                
                do {
                    let signUpResponse = try decoder.decode(SignUpResponse.self, from: result.data)
                    completion(signUpResponse, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil,error)
            }
        }
    }
    
    public func createPin(pin: String, completion: @escaping (CreatePinResponse?, Error?) -> ()) {
        let provider = MoyaProvider<AuthApi> ()
        provider.request(.createPin(pin: pin)) { response in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let createPinResponse = try decoder.decode(CreatePinResponse.self, from: result.data)
                    completion(createPinResponse, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil,error)
            }
        }
    }
    
    public func setOtp(email: String, otp: String, completion: @escaping (CommonResponse?, Error?) -> ()) {
        let provider = MoyaProvider<AuthApi> ()
        provider.request(.setOtp(email: email, otp: otp)){ response in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let setOtpResponse = try decoder.decode(CommonResponse.self, from: result.data)
                    completion(setOtpResponse, nil)
                } catch let error {
                    completion(nil, error)
                    
                }
            case .failure(let error):
                completion(nil,error)
            }
        }

    }
    public func checkPin(pin: String, completion: @escaping (CommonResponse?, Error?) -> ()) {
        let provider = MoyaProvider<AuthApi> ()
        provider.request(.checkPin(pin: pin)) {response in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let setOtpResponse = try decoder.decode(CommonResponse.self, from: result.data)
                    completion(setOtpResponse, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil,error)
            }
            
        }
    }
    
}
