//
//  AuthApi.swift
//  Core
//
//  Created by MacBook on 24/05/21.
//

import Foundation
import Moya

public enum AuthApi {
    case login(email: String, password: String)
    case signup(username: String, email:String, password: String)
    case createPin(pin: String)
    case setOtp(email: String, otp: String)
    case checkPin(pin: String)
    case refreshToken(email: String, refreshToken: String)
    case transfer(receiver: Int, amount: Int, notes: String, pin:String)
    
}

extension AuthApi: TargetType {
    public var baseURL: URL {
        return URL(string: AppConstant.baseUrl)!
    }
    
    public var method: Moya.Method {
        switch self {
        case .createPin:
            return .patch
        case .setOtp:
            return .get
        default:
            return .post
        }
         
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .login(let email, let password):
            return .requestParameters(parameters: ["email": email,
                                                   "password": password],
                                                    encoding: JSONEncoding.default)
        case .signup(let username, let email, let password):
            return .requestParameters(parameters: ["username": username,
                                                   "email": email,
                                                   "password": password],
                                                    encoding: JSONEncoding.default)
        case .createPin(let pin):
            return .requestParameters(parameters: ["PIN": pin], encoding: JSONEncoding.default)
        case .setOtp:
            return .requestPlain
        case .checkPin:
            return .requestPlain
        case .refreshToken(let email, let refreshToken):
            return .requestParameters(parameters: ["email": email,
                                                   "refreshToken": refreshToken],
                                                    encoding: JSONEncoding.default)
        case .transfer(let receiver, let amount, let notes, _):
            return .requestParameters(parameters: ["receiver": receiver,
                                                   "amount": amount,
                                                   "notes": notes],
                                                    encoding: JSONEncoding.default)
        }
    }
    
    public var headers: [String : String]? {
        let token: String = UserDefaultHelper.shared.get(key: .userToken) ?? ""
        switch self {
        case .createPin:
            return [
                "Conten-Type":"application/json",
                "Authorization":"Bearer \(token)"
                ]
        case .refreshToken:
            return [
                    "Conten-Type":"application/json",
                    "Authorization":"Bearer \(token)"
                ]
        case .transfer(_, _, _, let pin):
            return [
                "Content-Type": "application/json",
                "Authorization": "Bearer \(token)",
                "x-access-PIN": "\(pin)"
        
                ]
        default:
            return [
                "Conten-Type":"application/json"
            ]

        }
    }
    public var path: String {
        switch self {
        case .login:
            return "auth/login"
        case .signup:
            return "auth/signup"
        case .setOtp(let email, let otp):
            return "auth/activate/\(email)/\(otp)"
        case .createPin:
//            let otp: String = UserDefaultHelper.shared.get(key: .email) as! String
            return "auth/PIN"
        case .checkPin(let pin):
            return "auth/checkPIN/\(pin)"
        case .refreshToken:
            return "auth/refresh-token"
        case .transfer:
            return "tranfer/newTranfer"
        }
    }

}

