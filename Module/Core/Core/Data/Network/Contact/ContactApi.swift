//
//  ContactApi.swift
//  Core
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Moya

public enum ContactApi {
    case getContactUser
    
}

extension ContactApi: TargetType {
    public var baseURL: URL {
        return URL(string: AppConstant.baseUrl)!
    }
    
    public var path: String {
        switch self {
        case .getContactUser:
            return "/tranfer/contactUser"
        }
    }
    
    public var method: Moya.Method {
        return .get
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .getContactUser:
            return .requestPlain
            
        }
    }
    
    public var headers: [String : String]? {
        let token: String = UserDefaultHelper.shared.get(key: .userToken) ?? ""
        return [
            "Conten-Type":"application/json",
            "Authorization":"Bearer \(token)"
        ]
    }
    
    
}
