//
//  ContactNetworkManagerImpl.swift
//  Core
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Moya

public class ContactNetworkManagerImpl: ContactNetworkManager {
    public func getAllContacts(completion: @escaping ([ContactDataResponse]?, Error?) -> ()) {
        let provider = MoyaProvider<ContactApi>()
        provider.request(.getContactUser) { response in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let getInvoiceResponse = try decoder.decode(ContactResponse.self, from: result.data)
                    completion(getInvoiceResponse.data, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    

    
    
    public init() {
        
    }
    
    
}
