//
//  TransferEntity.swift
//  Core
//
//  Created by MacBook on 28/05/21.
//

import Foundation

public struct TransferEntity {
    public var receiverData: ProfileEntity
    public var amount: Int
    public var balance: Int
    public var date: String
    public var notes: String
    public var pin: String
    
    public init(receiverData: ProfileEntity, amount: Int, balance: Int, date: String, notes: String, pin: String) {
        self.receiverData = receiverData
        self.amount = amount
        self.balance = balance
        self.date = date
        self.notes = notes
        self.pin = pin
    }
}
