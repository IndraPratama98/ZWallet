//
//  BalanceEntity.swift
//  Core
//
//  Created by MacBook on 30/05/21.
//

import Foundation

public struct BalanceEntity {
    public var balanceLeft: Int
    
    public init(balanceLeft: Int) {
        self.balanceLeft = balanceLeft
    }
}
