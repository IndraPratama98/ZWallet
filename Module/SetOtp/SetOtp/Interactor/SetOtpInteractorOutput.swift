//
//  SetOtpInteractorOutput.swift
//  SetOtp
//
//  Created by MacBook on 29/05/21.
//

import Foundation

protocol SetOtpInteractorOutput {
    func authenticationResult(isSuccess: Bool, status: Int, message: String)
}



