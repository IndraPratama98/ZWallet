//
//  SetOtpInteractorImpl.swift
//  SetOtp
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import Core

public class SetOtpInteractorImpl: SetOtpInteractor {
    var interactorOutput: SetOtpInteractorOutput?
    let authNetworkManager: AuthNetworkManager
    
    init(networkManager: AuthNetworkManager) {
        self.authNetworkManager = networkManager
    }
    
    
    func setOtp(otp: String, email: String) {
        self.authNetworkManager.setOtp(email: email, otp: otp ) {(data, error) in
            
            if let dataResponse = data {
                if data?.status == 200 {
                    self.interactorOutput?.authenticationResult(isSuccess: true, status: data?.status ?? 0, message: data?.message ?? "")
                } else if data?.status == 404 {
                    self.interactorOutput?.authenticationResult(isSuccess: false, status: data?.status ?? 0, message: data?.message ?? "")
                }
                
            } else {
                self.interactorOutput?.authenticationResult(isSuccess: true, status: data?.status ?? 0, message: data?.message ?? "")
            }
        }
    }
    
}


