//
//  SetOtpInteractor.swift
//  SetOtp
//
//  Created by MacBook on 29/05/21.
//

import Foundation

protocol SetOtpInteractor {
    func setOtp(otp: String, email: String)
}
