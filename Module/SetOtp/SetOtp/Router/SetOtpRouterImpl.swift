//
//  SetOtpRouterImpl.swift
//  SetOtp
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import Core
import UIKit

public class SetOtpRouterImpl: SetOtpRouter {
    func navigateToLogin() {
        AppRouter.shared.navigateToLogin()
    }
    
    public static func navigateToModule(email: String){
        let bundle = Bundle(identifier: "com.casestudy.SetOtp")
        
        let vc = SetOtpViewController(nibName: String(describing: SetOtpViewController.self), bundle: bundle)
        
        let router = SetOtpRouterImpl()
        let networkManager = AuthNetworkmanagerImpl()
        let interactor = SetOtpInteractorImpl(networkManager: networkManager)
        let presenter = SetOtpPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter 
        vc.presenter = presenter
        vc.email = email
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
    
}
