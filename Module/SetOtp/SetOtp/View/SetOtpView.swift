//
//  SetOtpView.swift
//  SetOtp
//
//  Created by MacBook on 29/05/21.
//

import Foundation

protocol SetOtpView {
    func showError()
    func showSuccess()
}
