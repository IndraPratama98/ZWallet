//
//  SetOtpViewController.swift
//  SetOtp
//
//  Created by MacBook on 28/05/21.
//

import UIKit
import OTPFieldView

class SetOtpViewController: UIViewController, OTPFieldViewDelegate {
    
    @IBOutlet weak var otpTextFieldView: OTPFieldView!
    @IBOutlet weak var mainView: UIView!
    var otp: String = ""
    var email: String = ""
    var presenter: SetOtpPresenter?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupOtpView()
        mainView.dropShadowEffect()
    }
    
    
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        self.otp = otpString
    }
    
    func setupOtpView(){
        self.otpTextFieldView.fieldsCount = 6
        self.otpTextFieldView.fieldBorderWidth = 2
        self.otpTextFieldView.defaultBorderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.otpTextFieldView.filledBorderColor = #colorLiteral(red: 0.3882352941, green: 0.4745098039, blue: 0.9568627451, alpha: 1)
        self.otpTextFieldView.cursorColor = UIColor.red
        self.otpTextFieldView.displayType = .roundedCorner
        self.otpTextFieldView.fieldSize = 47
        self.otpTextFieldView.separatorSpace = 10
        self.otpTextFieldView.shouldAllowIntermediateEditing = false
        self.otpTextFieldView.delegate = self
        self.otpTextFieldView.initializeUI()
        
    }
    
    
    @IBAction func confirmOtpAction(_ sender: Any) {
        self.presenter?.setOtp(otp: otp, email: self.email)
    }
}

extension SetOtpViewController: SetOtpView {
    func showSuccess() {
        let alert = UIAlertController(title: "Berhasil", message: "Akun anda telah diaktifkan, silahkan login di halaman Login", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler:{(action:UIAlertAction!) in
            self.presenter?.navigateToLogin()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showError() {
        let alert = UIAlertController(title: "Peringatan", message: "Otp yang anda masukan salah", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Coba lagi", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
