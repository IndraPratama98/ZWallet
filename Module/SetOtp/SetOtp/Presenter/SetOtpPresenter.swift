//
//  SetOtpPresenter.swift
//  SetOtp
//
//  Created by MacBook on 29/05/21.
//

import Foundation

protocol SetOtpPresenter {
    func setOtp(otp: String, email: String)
    func navigateToLogin()
}
