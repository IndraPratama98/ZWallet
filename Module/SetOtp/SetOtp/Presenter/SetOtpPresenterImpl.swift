//
//  SetOtpPresenterImpl.swift
//  SetOtp
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import Core


class SetOtpPresenterImpl: SetOtpPresenter {
    func navigateToLogin() {
        self.router.navigateToLogin()
    }
    

        var view: SetOtpView
        var interactor: SetOtpInteractor
        var router: SetOtpRouter
        
        init(view: SetOtpView, interactor: SetOtpInteractor, router: SetOtpRouter) {
            self.view = view
            self.interactor = interactor
            self.router = router
        }
        
    func setOtp(otp: String, email: String) {
        self.interactor.setOtp(otp: otp, email: email)
    }
    
    
}

extension SetOtpPresenterImpl: SetOtpInteractorOutput {
    func authenticationResult(isSuccess: Bool, status: Int, message: String) {
        if isSuccess {
            switch status {
            case 200:
                self.view.showSuccess()
            default:
                print("Test")
            }
        } else {
            self.view.showError()
        }
    }
}
