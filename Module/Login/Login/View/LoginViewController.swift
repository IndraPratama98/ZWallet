//
//  LoginViewController.swift
//  Login
//
//  Created by MacBook on 24/05/21.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var emailImage: UIImageView!
    @IBOutlet weak var lockImage: UIImageView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var eyeCrossed: UIButton!
    
    var presenter: LoginPresenter?
    var iconClick: Bool = true
    
    //    eye-crossed
    override func viewDidLoad() {
        super.viewDidLoad()
        
        passwordText.isSecureTextEntry = true
        let email = UIImage(named: "mail", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil)
        let lock = UIImage(named: "lock", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil)
        eyeCrossed.setImage(UIImage(named: "eye-crossed", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil), for: .normal) 
        
        emailImage.image = lock
        lockImage.image = email
        mainView.dropShadowEffect()
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    
    @IBAction func loginAction(_ sender: Any) {
        let email: String = emailText.text ?? ""
        let password: String = passwordText.text ?? ""
        self.presenter?.login(email: email, password: password)
    }
    
    
    @IBAction func signUpAction(_ sender: Any) {
        self.presenter?.navigateToSignUp()
    }
    
    @IBAction func showPassword(_ sender: Any) {
        if iconClick == true {
            passwordText.isSecureTextEntry = false
            eyeCrossed.tintColor = #colorLiteral(red: 0.4462532401, green: 0.5604025126, blue: 0.998419106, alpha: 1)
        }else{
            passwordText.isSecureTextEntry = true
            eyeCrossed.tintColor = #colorLiteral(red: 0.6825018525, green: 0.6820691824, blue: 0.6994950175, alpha: 1)
        }
        
        iconClick = !iconClick
    }
}
func addLeftImage(txtField:UITextField, andImage img:UIImage){
    let leftImageview = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: 20, height: 100))
    leftImageview.image = img
    txtField.leftView = leftImageview
    txtField.leftViewMode = .always
    txtField.frame = CGRect(x: 100, y: 0, width: 10, height: 10)
    
}



extension UITextField {
    func setLeftPaddingPoints( amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints( amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}



extension LoginViewController: LoginView {
    func showError() {
        let alert = UIAlertController(title: "Peringatan", message: "Username atau password salah, silahkan coba lagi", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}



