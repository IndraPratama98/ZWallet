//
//  LoginPresenterImpl.swift
//  Login
//
//  Created by MacBook on 24/05/21.
//

import Foundation

class LoginPresenterImpl: LoginPresenter {
    
    
    var view: LoginView
    var interactor: LoginInteractor
    var router: LoginRouter
    
    init(view: LoginView, interactor: LoginInteractor, router: LoginRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func login(email: String, password: String) {
        self.interactor.postLoginData(email: email, password: password)
    }
    
    func navigateToSignUp() {
        self.router.navigateToSignUp()
    }
}

extension LoginPresenterImpl: LoginInteractorOutput {
    func authenticationResult(isSuccess: Bool, status: Int, message: String, hassPin: Bool) {
        if isSuccess {
            switch hassPin {
            case true:
                self.router.navigateToHome()
            case false:
                self.router.navigateToCreatePin()
            }
        } else {
            self.view.showError()
        }
    }
    
  
    
}
