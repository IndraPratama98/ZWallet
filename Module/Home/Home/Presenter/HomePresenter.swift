//
//  HomePresenter.swift
//  Home
//
//  Created by MacBook on 25/05/21.
//

import Foundation
import UIKit

protocol HomePresenter: class {
    func loadProfile()
    func loadTransaction()
    func showContact(viewController: UIViewController)
    func showHistory(viewController: UIViewController)
    func showTopUp(viewController: UIViewController)
    func logout()
}
