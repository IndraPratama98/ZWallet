//
//  HomePresenterImpl.swift
//  Home
//
//  Created by MacBook on 25/05/21.
//

import Foundation
import Core
import UIKit

class HomePresenterImpl: HomePresenter {
    func showTopUp(viewController: UIViewController) {
        self.router.navigateToTopUp(viewController: viewController)
    }
    
    func showContact(viewController: UIViewController) {
        self.router.navigateToContact(viewController: viewController)
    }
    
    
    
    let view: HomeView
    let interactor: HomeInteractor
    let router: HomeRouter
    
    init(view: HomeView, interactor: HomeInteractor, router: HomeRouter){
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func loadProfile() {
        self.interactor.getUserProfile()
    }
    
    func loadTransaction() {
        self.interactor.getTransaction()
    }
    
    func showHistory(viewController: UIViewController) {
        self.router.navigateToHistory(viewController: viewController)
    }
    
    func logout() {
        UserDefaultHelper.shared.remove(key: .userToken)
        UserDefaultHelper.shared.remove(key: .refreshToken)
        UserDefaultHelper.shared.remove(key: .email)
        self.router.navigateToLogin()
    }
}

extension HomePresenterImpl: HomeInteractorOutput {
    func loadedUserProfileDate(userProfile: UserProfileEntity) {
        self.view.showUserProfileData(userProfile: userProfile)
    }
    
    func loadedTransaction(transactions: [TransactionEntity]) {
        self.view.showTransactionData(transactions: transactions)
    }
    
    
}
