//
//  HomeViewController.swift
//  Home
//
//  Created by MacBook on 24/05/21.
//

import UIKit
import Core
import NVActivityIndicatorView

class HomeViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicatorView: NVActivityIndicatorView!
    
    
    var dataSource = HomeDataSource()
    var presenter: HomePresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.presenter?.loadProfile()
        self.presenter?.loadTransaction()
        activityIndicatorView.type = .lineScale
        activityIndicatorView.color = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
        activityIndicatorView.startAnimating()
        
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewDidLoad()
    }
    
    
    
    
    func setupTableView() {
        self.dataSource.viewController = self
        self.tableView.register(UINib(nibName: "DashboardCell", bundle: Bundle(identifier: "com.casestudy.Home")), forCellReuseIdentifier: "DashboardCell")
        self.tableView.register(UINib(nibName: "TransactionCell", bundle: Bundle(identifier: "com.casestudy.Core")), forCellReuseIdentifier: "TransactionCell")
        self.tableView.register(UINib(nibName: "EmptyCell", bundle: Bundle(identifier: "com.casestudy.Core")), forCellReuseIdentifier: "EmptyCell")
        self.tableView.dataSource = self.dataSource
    }
}

extension HomeViewController: DashboardCellDelegate {
    func showTopUp() {
        self.presenter?.showTopUp(viewController: self)
    }
    
    func showAllContact() {
        self.presenter?.showContact(viewController: self)
    }
    
    
    func logout() {
        self.presenter?.logout()
    }
    
    func showAllTransaction() {
        self.presenter?.showHistory(viewController: self)
    }
    
    
}

extension HomeViewController: HomeView{
    func showUserProfileData(userProfile: UserProfileEntity) {
        self.dataSource.userProfile = userProfile
        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
            self.activityIndicatorView.stopAnimating()
            self.tableView.reloadData()
        }
        
    }
    
    func showTransactionData(transactions: [TransactionEntity]) {
        self.dataSource.transactions = transactions
        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
            self.activityIndicatorView.stopAnimating()
            self.tableView.reloadData()
        }
        
    }
    
    
}
