//
//  DashboardCell.swift
//  Home
//
//  Created by MacBook on 24/05/21.
//

import UIKit
import Core

class DashboardCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var logoutButton: UIButton!
    
    var delegate: DashboardCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

    public func showData(userProfile: UserProfileEntity) {
        self.nameLabel.text = userProfile.name
        self.balanceLabel.text = userProfile.balance.formatToIdr()
        self.phoneNumberLabel.text = userProfile.phoneNumber
        let url = URL(string: userProfile.imageUrl)
        self.profileImage.kf.setImage(with: url)
        
        logoutButton.setImage(UIImage(named: "bell", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil), for: .normal)
        
    }
    
    @IBAction func showContactAction(_ sender: Any) {
        self.delegate?.showAllContact()
    }
    
    
    @IBAction func logoutAction(_ sender: Any) {
        self.delegate?.logout()
    }
    
    @IBAction func showTransactionAction(_ sender: Any) {
        self.delegate?.showAllTransaction()
    }
    
    @IBAction func topUpAction(_ sender: Any) {
        self.delegate?.showTopUp()
    }
}
