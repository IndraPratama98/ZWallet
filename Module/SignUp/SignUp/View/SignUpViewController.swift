//
//  SignUpViewController.swift
//  SignUp
//
//  Created by MacBook on 25/05/21.
//

import UIKit

class SignUpViewController: UIViewController {

    var presenter: SignUpPresenter?
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var emailImage: UIImageView!
    @IBOutlet weak var passwordImage: UIImageView!
    
    
    @IBOutlet weak var userLine: UIView!
    @IBOutlet weak var emailLine: UIView!
    @IBOutlet weak var passwordLine: UIView!
    @IBOutlet weak var mainView: UIView!
    var iconClick: Bool = true
    
    @IBOutlet weak var eyeCrossed: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordText.isSecureTextEntry = true
        mainView.dropShadowEffect()
        userImage.image = UIImage(named: "person-1", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil)
        emailImage.image = UIImage(named: "mail", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil)
        passwordImage.image = UIImage(named: "lock", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil)
        eyeCrossed.setImage(UIImage(named: "eye-crossed", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil), for: .normal)
        
    }
    
    
    @IBAction func eyeCrossedButton(_ sender: Any) {
        if iconClick == true {
            passwordText.isSecureTextEntry = false
            eyeCrossed.tintColor = #colorLiteral(red: 0.4462532401, green: 0.5604025126, blue: 0.998419106, alpha: 1)
        }else{
            passwordText.isSecureTextEntry = true
            eyeCrossed.tintColor = #colorLiteral(red: 0.6825018525, green: 0.6820691824, blue: 0.6994950175, alpha: 1)
        }
        
        iconClick = !iconClick
    }
    
    
    @IBAction func signUpAction(_ sender: Any) {
        let username = usernameText.text ?? ""
        let email = emailText.text ?? ""
        let password = passwordText.text ?? ""
        self.presenter?.signUp(username: username, email: email, password: password)
    }
    
    @IBAction func navigateToLoginAction(_ sender: Any) {
        self.presenter?.navigateToLogin()
    }
    
    
}

 
extension SignUpViewController: SignUpView {
    func showSuccess() {
        let alert = UIAlertController(title: "Sukses", message: "Silahkan lakukan aktivasi OTP", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler:{(action:UIAlertAction!) in
            self.presenter?.navigateToSetOtp(email: self.emailText.text ?? "")
        }))
        self.present(alert, animated: true, completion: nil)

    }
    
    func showError() {
        let alert = UIAlertController(title: "Gagal", message: "Email telah digunakan", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
}


