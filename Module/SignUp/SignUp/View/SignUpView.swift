//
//  SignUpView.swift
//  SignUp
//
//  Created by MacBook on 25/05/21.
//

import Foundation

protocol SignUpView {
    func showError()
    func showSuccess()
}
