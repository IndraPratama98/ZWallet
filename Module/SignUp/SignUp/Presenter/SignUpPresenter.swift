//
//  LoginPresenter.swift
//  SignUp
//
//  Created by MacBook on 26/05/21.
//

import Foundation

protocol SignUpPresenter {
    func signUp(username: String, email: String, password: String)
    func navigateToLogin()
    func navigateToSetOtp(email:String)
}
