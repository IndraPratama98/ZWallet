//
//  LoginPresenterImpl.swift
//  SignUp
//
//  Created by MacBook on 26/05/21.
//

import Foundation


class SignUpPresenterImpl: SignUpPresenter {
    func navigateToSetOtp(email: String) {
        self.router.navigateToSetOtp(email: email)
    }
    
    
    let view: SignUpView
    let interactor: SignUpInteractor
    let router: SignUpRouter
    
    init(view: SignUpView, interactor: SignUpInteractor, router: SignUpRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func signUp(username: String, email: String, password: String) {
        self.interactor.postSignUpData(username: username, email: email, password: password)
    }
    
    func navigateToLogin() {
        self.router.navigateToLogin()
    }

    
}

extension SignUpPresenterImpl: SignUpInteractorOutput {
    func authenticationResult(isSuccess: Bool, message: String, status: Int) {
        if isSuccess {
            switch status {
            case 200:
                self.view.showSuccess()
            case 401:
                self.view.showError()
            default:
                print("Test")
            }
        } else {
            self.view.showError()
        }
    }
    
    
    

    
    
}
