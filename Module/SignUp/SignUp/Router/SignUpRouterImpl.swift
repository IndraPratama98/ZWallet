//
//  SignUpRouterImpl.swift
//  SignUp
//
//  Created by MacBook on 26/05/21.
//

import Foundation
import UIKit
import Core

public class SignUpRouterImpl {
    public static func navigateToModule() {
//    Untuk menampilkan halaman signup
        let bundle = Bundle(identifier: "com.casestudy.SignUp")
        let vc = SignUpViewController(nibName: String(describing: SignUpViewController.self), bundle: bundle)
        let router = SignUpRouterImpl()
        let networkManager = AuthNetworkmanagerImpl()
        let interactor = SignUpInteractorImpl(authNetworkManager: networkManager)
        
        let presenter = SignUpPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}

extension SignUpRouterImpl: SignUpRouter {
    func navigateToLogin() {
        AppRouter.shared.navigateToLogin()
    }
    
    func navigateToSetOtp(email: String) {
        AppRouter.shared.navigateToSetOtp(email)
    }
}
