//
//  SignUpRouter.swift
//  SignUp
//
//  Created by MacBook on 26/05/21.
//

import Foundation

protocol SignUpRouter {
    func navigateToSetOtp(email:String)
    func navigateToLogin()
}
