//
//  SignUpInteractorImpl.swift
//  SignUp
//
//  Created by MacBook on 26/05/21.
//

import Foundation
import Core

class SignUpInteractorImpl: SignUpInteractor {
    
    var interactorOutput: SignUpInteractorOutput?
    let authNetworkManager: AuthNetworkManager
    
    init(authNetworkManager: AuthNetworkManager) {
        self.authNetworkManager = authNetworkManager
    }
    
    func postSignUpData(username: String, email: String, password: String) {
        self.authNetworkManager.signUp(username: username, email: email, password: password) { (data , error) in
            if let dataResponse = data {
                if data?.status == 200 {
                    self.interactorOutput?.authenticationResult(isSuccess: true, message: data?.message ?? "", status: data?.status ?? 0)
                } else if data?.status == 401 {
                    self.interactorOutput?.authenticationResult(isSuccess: false, message: data?.message ?? "", status: data?.status ?? 0)
                }
            } else {
                self.interactorOutput?.authenticationResult(isSuccess: false, message: data?.message ?? "", status: data?.status ?? 0)
            }
        }
    
    }
}
