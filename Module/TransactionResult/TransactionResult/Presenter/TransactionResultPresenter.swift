//
//  TransactionResultPresenter.swift
//  TransactionResult
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import UIKit
import Core

protocol TransactionResultPresenter {
    func loadTransactionResult()
    func navigateToHome(viewController: UIViewController)
}
