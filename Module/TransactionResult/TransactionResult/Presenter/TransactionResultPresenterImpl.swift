//
//  TransactionResultPresenterImpl.swift
//  TransactionResult
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import UIKit

public class TransactionResultPresenterImpl: TransactionResultPresenter {
    func loadTransactionResult() {
        
    }
    
    func navigateToHome(viewController: UIViewController) {
        self.router.navigateToHome(viewController: viewController)
    }
    
    let view: TransactionResultView
    let interactor: TransactionResultInteractor
    let router: TransactionResultRouter
    
    init(view: TransactionResultView, interactor: TransactionResultInteractor, router: TransactionResultRouter){
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    
}
