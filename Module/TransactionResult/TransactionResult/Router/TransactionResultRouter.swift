//
//  TransactionResultRouter.swift
//  TransactionResult
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import UIKit

protocol TransactionResultRouter {
    func navigateToHome(viewController: UIViewController)
}
