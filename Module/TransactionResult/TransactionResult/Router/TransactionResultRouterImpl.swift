//
//  TransactionResultRouterImpl.swift
//  TransactionResult
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import UIKit
import Core

public class TransactionResultRouterImpl: TransactionResultRouter {
    func navigateToHome(viewController: UIViewController) {
        viewController.navigationController?.popToRootViewController(animated: true)
    }
    
    
    public static func navigateToModule(viewController: UIViewController, transferData: TransferEntity, status: Bool){
        let bundle = Bundle(identifier: "com.casestudy.TransactionResult")
        let vc = TransactionResultViewController(nibName: String(describing: TransactionResultViewController.self), bundle: bundle)
        let router = TransactionResultRouterImpl()
        let interactor = TransactionResultInteractorImpl()
        
        let presenter = TransactionResultPresenterImpl(view: vc, interactor: interactor, router: router)
        
        vc.presenter = presenter
        vc.status = status
        vc.transferData = transferData
        
        viewController.navigationController?.setNavigationBarHidden(true, animated: false)
        viewController.navigationController?.pushViewController(vc, animated: true)
    }
}
