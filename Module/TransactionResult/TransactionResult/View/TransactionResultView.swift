//
//  TransactionResultView.swift
//  TransactionResult
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core

protocol TransactionResultView {
    func showTransactionResult(transferData:TransferEntity)
}
