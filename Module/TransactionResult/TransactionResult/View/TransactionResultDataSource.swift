//
//  TransactionResultDataSource.swift
//  TransactionResult
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import UIKit
import Core


class TransactionResultDataSource: NSObject, UITableViewDataSource {
    
    var viewController: TransactionResultViewController!
    var transferData: TransferEntity = TransferEntity(receiverData: ProfileEntity(id: 0, name: "", phone: "", image: ""), amount: 0, balance: 0, date: "", notes: "", pin: "")
    var status: Bool = false
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionResultCell", for: indexPath) as! TransactionResultCell
        cell.showData(transferData: self.transferData, status: status)
        cell.delegate = self.viewController
        return cell
    }
}
