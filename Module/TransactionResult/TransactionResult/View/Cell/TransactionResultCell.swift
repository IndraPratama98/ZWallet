//
//  TransactionResultCell.swift
//  TransactionResult
//
//  Created by MacBook on 29/05/21.
//

import UIKit
import Core
import Kingfisher

class TransactionResultCell: UITableViewCell {

    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var notesLabel: UILabel!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var backToHome: UIButton!
    
    @IBOutlet weak var amountView: UIView!
    @IBOutlet weak var balanceView: UIView!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var notesView: UIView!
    @IBOutlet weak var contactView: UIView!
    
    
    var delegate: TransactionResultCellDelegate?
    
    public func showData(transferData: TransferEntity, status: Bool) {
        statusView.layer.cornerRadius = statusView.frame.size.width / 2
        
        amountView.dropShadowEffect()
        balanceView.dropShadowEffect()
        dateView.dropShadowEffect()
        notesView.dropShadowEffect()
        contactView.dropShadowEffect()
        
        self.profileNameLabel.text = transferData.receiverData.name
        self.phoneLabel.text = transferData.receiverData.phone
        self.dateLabel.text = transferData.date
        self.amountLabel.text = transferData.amount.formatToIdr()
        self.balanceLabel.text = transferData.balance.formatToIdr()
        self.notesLabel.text = transferData.notes
        
        self.profileImage.kf.setImage(with: URL(string: transferData.receiverData.image))
        if status {
            self.statusView.backgroundColor = #colorLiteral(red: 0, green: 0.7755809426, blue: 0.318590939, alpha: 1)
            self.informationLabel.text = ""
            self.statusImage.image = UIImage(named: "success", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil)
            self.statusLabel.text = "Transfer Success"
            self.backToHome.setTitle("Back to Home", for: .normal)
        } else {
            self.statusView.backgroundColor = #colorLiteral(red: 1, green: 0.2915042639, blue: 0.1234962121, alpha: 1)
            self.informationLabel.text = "We can’t transfer your money at the moment, we recommend you to check your internet connection and try again."
            self.statusImage.image = UIImage(named: "failed", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil)
            self.statusLabel.text = "Transfer Failed"
            self.backToHome.setTitle("Try Again", for: .normal)
        }
        
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    @IBAction func backToHome(_ sender: Any) {
        self.delegate?.navigateToHome()
    }
}


