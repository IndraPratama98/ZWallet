//
//  TransactionResultViewController.swift
//  TransactionResult
//
//  Created by MacBook on 28/05/21.
//

import UIKit
import Core

class TransactionResultViewController: UIViewController{
    
    var dataSource = TransactionResultDataSource()
    var presenter: TransactionResultPresenter?
    
    @IBOutlet weak var tableView: UITableView!
    
    var transferData = TransferEntity(receiverData: ProfileEntity(id: 0, name: "", phone: "", image: ""), amount: 0, balance: 0, date: "", notes: "", pin: "")
    var status: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
    }
    
    func setupTableView() {
        self.dataSource.transferData = transferData
        self.dataSource.status = status
        self.dataSource.viewController = self
        let bundle = Bundle(identifier: "com.casestudy.TransactionResult")
        self.tableView.register(UINib(nibName: "TransactionResultCell", bundle: bundle), forCellReuseIdentifier: "TransactionResultCell")
        self.tableView.dataSource = self.dataSource
    }
    
}

extension TransactionResultViewController: TransactionResultView {
    func showTransactionResult(transferData: TransferEntity) {
        self.tableView.reloadData()
    }
}

extension TransactionResultViewController: TransactionResultCellDelegate {
    func navigateToHome() {
        self.presenter?.navigateToHome(viewController: self)
    }
}
