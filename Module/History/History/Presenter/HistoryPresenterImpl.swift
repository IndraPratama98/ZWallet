//
//  HistoryPresenterImpl.swift
//  History
//
//  Created by MacBook on 25/05/21.
//

import Foundation
import Core
import UIKit

class HistoryPresenterImpl: HistoryPresenter {
    
    let view: HistoryView
    let interactor: HistoryInteractor
    let router: HistoryRouter
    
    init(view: HistoryView, interactor: HistoryInteractor, router: HistoryRouter){
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func loadTransaction() {
        self.interactor.getAllTransaction()
    }
    
    func backToHistory(viewController: UIViewController) {
        self.router.navigateToHome(viewController: viewController)
    }
}


extension HistoryPresenterImpl: HistoryInteractorOutput {
    func loadedTransaction(transactions: [TransactionEntity]) {
        self.view.showTransactionData(transactions: transactions)
    }
    
    
}
