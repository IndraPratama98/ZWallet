//
//  HistoryInteractor.swift
//  History
//
//  Created by MacBook on 25/05/21.
//

import Foundation
import UIKit
import Core

protocol HistoryInteractor {
    func getAllTransaction()
}
