//
//  HistoryViewController.swift
//  History
//
//  Created by MacBook on 25/05/21.
//

import UIKit
import Core
import NVActivityIndicatorView

class HistoryViewController: UIViewController {

    var dataSource = HistoryDataSource()
    var presenter: HistoryPresenter?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var outcomeButton: UIButton!
    @IBOutlet weak var incomeButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    
    var transactions: [TransactionEntity] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadingIndicator.type = .lineScale
        loadingIndicator.color = #colorLiteral(red: 0.3857212365, green: 0.4749737978, blue: 0.9563460946, alpha: 1)
        loadingIndicator.startAnimating()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            self.setupTableView()
            self.presenter?.loadTransaction()
            self.setImage()
            self.loadingIndicator.stopAnimating()
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func setupTableView() {
        self.dataSource.viewController = self
        self.tableView.register(UINib(nibName: "TransactionCell", bundle: Bundle(identifier: "com.casestudy.Core")), forCellReuseIdentifier: "TransactionCell")
        
        self.tableView.register(UINib(nibName: "EmptyCell", bundle: Bundle(identifier: "com.casestudy.Core")), forCellReuseIdentifier: "EmptyCell")
        self.tableView.dataSource = self.dataSource
        
        
    }

    @IBAction func backToHomeAction(_ sender: Any) {
        self.presenter?.backToHistory(viewController: self)
    }
    
    @IBAction func selectByOutcome(_ sender: Any) {
        outcomeButton.backgroundColor = #colorLiteral(red: 0.3807878494, green: 0.4607550502, blue: 0.9897784591, alpha: 1)
        incomeButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//        outcomeButton.setImage(UIImage(named: "arrow_down.png"), for: .normal)
        self.outcomeButton.setImage(UIImage(named: "arrow_up", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil), for: .normal)
        self.incomeButton.setImage(UIImage(named: "pemasukan", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil), for: .normal)
        
    }
    
    fileprivate func extractedFunc() {
        incomeButton.backgroundColor = #colorLiteral(red: 0.3807878494, green: 0.4607550502, blue: 0.9897784591, alpha: 1)
        outcomeButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        self.incomeButton.setImage(UIImage(named: "arrow_down", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil), for: .normal)
        self.outcomeButton.setImage(UIImage(named: "pengeluaran", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil), for: .normal)
    }
    
    @IBAction func selectByIncome(_ sender: Any) {
        extractedFunc()
    }
    
    fileprivate func setImage() {
        self.backButton.setImage(UIImage(named: "arrow-left", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil), for: .normal)
        self.incomeButton.setImage(UIImage(named: "pemasukan", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil), for: .normal)
        self.outcomeButton.setImage(UIImage(named: "pengeluaran", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil), for: .normal)
    }
    
}

extension HistoryViewController: HistoryView {
    func showTransactionData(transactions: [TransactionEntity]) {
        self.dataSource.transactions = transactions
        self.tableView.reloadData()
    }
}
