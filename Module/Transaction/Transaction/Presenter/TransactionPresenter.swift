//
//  TransactionPresenter.swift
//  Transaction
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import UIKit
import Core

protocol TransactionPresenter {
    func backToHome(viewController: UIViewController)
    func navigateToContinue(viewController: UIViewController, transferData: TransferEntity)
}
