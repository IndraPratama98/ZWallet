//
//  TransactionPresenterImpl.swift
//  Transaction
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import UIKit
import Core

public class TransactionPresenterImpl: TransactionPresenter {
    func navigateToContinue(viewController: UIViewController, transferData: TransferEntity) {
        self.router.navigateToContinueTransaction(viewController: viewController, transferData: transferData)
    }
    
    func backToHome(viewController: UIViewController) {
        self.router.backToTransfer(viewController: viewController)
    }
    
    
    let view: TransactionView
    let interactor: TransactionInteractor
    let router: TransactionRouter
    
    init(view: TransactionView, interactor: TransactionInteractor, router: TransactionRouter){
        self.view = view
        self.interactor = interactor
        self.router = router
    }
}
