//
//  TransactionInteractor.swift
//  Transaction
//
//  Created by MacBook on 26/05/21.
//

import Foundation

protocol TransactionInteractor {
    func getConfiramationPin()
}
