//
//  TransactionView.swift
//  Transaction
//
//  Created by MacBook on 26/05/21.
//

import Foundation
import Core


protocol TransactionView {
    func showReceiverData(contacts: ProfileEntity)
}
