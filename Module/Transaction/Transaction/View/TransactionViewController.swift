//
//  TransactionViewController.swift
//  Transaction
//
//  Created by MacBook on 26/05/21.
//

import UIKit
import Core
import Kingfisher
import OTPFieldView

class TransactionViewController: UIViewController {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var amountText: UITextField!
    @IBOutlet weak var notesText: UITextField!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var test1: UILabel!
    @IBOutlet weak var editImage: UIImageView!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var contactView: UIView!
    @IBOutlet weak var nextButton: UIButton!
    
    var receiverData: ProfileEntity = ProfileEntity(id: 0, name: "", phone: "", image: "")
    var presenter: TransactionPresenter?
    var viewController: UIViewController?
    let balance: Int? = UserDefaultHelper.shared.get(key: .balanceLeft)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showData()
        
    }
    
    
    fileprivate func showData() {
        contactView.dropShadowEffect()
        notesText.addBottomBorder()
        let balance: Int? = UserDefaultHelper.shared.get(key: .balanceLeft)
        
        nextButton.isEnabled = false
        nextButton.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)

        self.nameLabel.text = receiverData.name
        self.phoneLabel.text = receiverData.phone
        let url = URL(string: receiverData.image)
        self.profileImage.kf.setImage(with: url)
        self.balanceLabel.text = "\(balance?.formatToIdr() ?? "") Available"
        amountText.addTarget(self, action: #selector(TransactionViewController.textFieldDidChange(_:)), for: .editingChanged)
        
        self.editImage.image = UIImage(named: "edit", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil)
                                       
        self.backButton.setImage(UIImage(named: "arrow-left", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil), for: .normal)
    }
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if amountText.text?.count == 0 {
            nextButton.isEnabled = false
            nextButton.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        } else {
            nextButton.isEnabled = true
            nextButton.backgroundColor = #colorLiteral(red: 0.3855632842, green: 0.4751614332, blue: 0.9522169232, alpha: 1)
        }
    }
    
    func showReceiverData(contacts: ProfileEntity) {
        self.nameLabel.text = contacts.name
        self.phoneLabel.text = contacts.phone
        let url = URL(string: contacts.image)
        self.profileImage.kf.setImage(with: url)
    }

    @IBAction func nextAction(_ sender: Any) {
        let dateNow: String  = "dd MMM yyyy - HH:mm"
        let amount = Int(amountText.text!)
        
        let transferData = TransferEntity(receiverData: receiverData, amount: amount ?? 0, balance: self.balance ?? 0, date: dateNow.dateFormat(), notes: notesText.text ?? "", pin: "")
        
        self.presenter?.navigateToContinue(viewController: self, transferData: transferData)
    }
    
    @IBAction func backToHome(_ sender: Any) {
        self.presenter?.backToHome(viewController: self)
    }
}

extension TransactionViewController: TransactionView {
    
    
}


