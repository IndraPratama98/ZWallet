//
//  TransactionRouterImpl.swift
//  Transaction
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import UIKit
import Core

public class TransactionRouterImpl {
    
    
    public static func navigateToModule(viewController: UIViewController, receiverData: ProfileEntity){
        let bundle = Bundle(identifier: "com.casestudy.Transaction")
        let vc = TransactionViewController(nibName: String(describing: TransactionViewController.self), bundle: bundle)
        
        let router = TransactionRouterImpl()
        let interactor = TransactionInteractorImpl()
        let presenter = TransactionPresenterImpl(view: vc, interactor: interactor, router: router)
      
//        interactor.interactorOutput = presenter
        
        vc.presenter = presenter
        vc.receiverData = receiverData
        
        
        
        viewController.navigationController?.setNavigationBarHidden(true, animated: true)
        viewController.navigationController?.pushViewController(vc, animated: true)
        
        
//        UIApplication.shared.windows.first?.rootViewController = vc
//        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}

extension TransactionRouterImpl: TransactionRouter {
    func navigateToContinueTransaction(viewController: UIViewController, transferData: TransferEntity) {
        AppRouter.shared.navigateToTransactionContinue(viewController, transferData)
    }
    
    func backToTransfer(viewController: UIViewController) {
//        AppRouter.shared.navigateToHome()
        viewController.navigationController?.popViewController(animated: true)
    }
}

