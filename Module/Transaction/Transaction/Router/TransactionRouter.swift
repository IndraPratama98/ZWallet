//
//  TransactionRouter.swift
//  Transaction
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import UIKit
import Core

protocol TransactionRouter {
    func navigateToContinueTransaction(viewController: UIViewController, transferData: TransferEntity)
    func backToTransfer(viewController: UIViewController)
}
