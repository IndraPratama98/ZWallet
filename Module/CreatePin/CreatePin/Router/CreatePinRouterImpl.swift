//
//  CreatePinRouterImpl.swift
//  CreatePin
//
//  Created by MacBook on 26/05/21.
//

import Foundation
import Core
import UIKit

public class CreatePinRouterImpl: CreatePinRouter {
    func navigateToLogin() {
        AppRouter.shared.navigateToLogin()
    }
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "com.casestudy.CreatePin")
        let vc = CreatePinViewController(nibName: String(describing: CreatePinViewController.self), bundle: bundle)
        let router = CreatePinRouterImpl()
        let networkManager = AuthNetworkmanagerImpl()
        let interactor = CreatePinInteractorImpl(authNetworkManager: networkManager)
        
        let presenter = CreatePinPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}
