//
//  CreatePinViewController.swift
//  CreatePin
//
//  Created by MacBook on 26/05/21.
//

import UIKit
import Core
import OTPFieldView

class CreatePinViewController: UIViewController, OTPFieldViewDelegate {
    
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        pin = otpString
    }
    
    
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var succesNotificationView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var buttonLogin: UIButton!
    @IBOutlet weak var otpTextFieldView: OTPFieldView!
    
    var presenter: CreatePinPresenter?
    var pin: String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupOtpView()
        succesNotificationView.isHidden = true
        mainView.dropShadowEffect()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setupOtpView()
    }
    
    
    @IBAction func confirmAction(_ sender: Any) {
        self.presenter?.createPin(pin: pin)
        
    }
    
    @IBAction func loginAction(_ sender: Any) {
        self.presenter?.navigateToLogin()
    }
    
    
    
    
    func setupOtpView(){
        self.otpTextFieldView.fieldsCount = 6
        self.otpTextFieldView.fieldBorderWidth = 2
        self.otpTextFieldView.defaultBorderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.otpTextFieldView.filledBorderColor = #colorLiteral(red: 0.3882352941, green: 0.4745098039, blue: 0.9568627451, alpha: 1)
        self.otpTextFieldView.cursorColor = UIColor.red
        self.otpTextFieldView.displayType = .roundedCorner
        self.otpTextFieldView.fieldSize = 47
        self.otpTextFieldView.separatorSpace = 10
        self.otpTextFieldView.shouldAllowIntermediateEditing = false
        self.otpTextFieldView.delegate = self
        self.otpTextFieldView.secureEntry = true
        self.otpTextFieldView.initializeUI()
        
    }
    
    
    
}

extension CreatePinViewController: CreatePinView {
    func showSuccess() {
        let alert = UIAlertController(title: "Berhasil", message: "Akun anda telah diaktifkan, silahkan login di halaman Login", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler:{(action:UIAlertAction!) in
            self.mainView.isHidden = true
            self.succesNotificationView.isHidden = false
            self.succesNotificationView.dropShadowEffect()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showError() {
        let alert = UIAlertController(title: "Peringatan", message: "Pin yang anda masukan tidak sesuai gunakan PIN 6 angka", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler:{(action:UIAlertAction!) in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

