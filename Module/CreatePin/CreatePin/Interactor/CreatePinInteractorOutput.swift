//
//  CreatePinInteractorOutput.swift
//  CreatePin
//
//  Created by MacBook on 26/05/21.
//

import Foundation
import Core

protocol CreatePinInteractorOutput {
    func authenticationResult(isSuccess: Bool, status: Int)
}
