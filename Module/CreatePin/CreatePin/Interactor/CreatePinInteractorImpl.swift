//
//  CreatePinInteractorImpl.swift
//  CreatePin
//
//  Created by MacBook on 26/05/21.
//

import Foundation
import Core


class CreatePinInteractorImpl: CreatePinInteractor {
    
    var interactorOutput: CreatePinInteractorOutput?
    let authNetworkManager: AuthNetworkManager
    
    init(authNetworkManager: AuthNetworkManager) {
        self.authNetworkManager = authNetworkManager
    }
    
    
    
    func postCreatePinData(pin: String) {
        self.authNetworkManager.createPin(pin: pin) { (data , error) in
            if let dataResponse = data {
                if data?.status == 200 {
                    self.interactorOutput?.authenticationResult(isSuccess: true, status: data?.status ?? 0)
                } else if data?.status == 500 {
                    self.interactorOutput?.authenticationResult(isSuccess: false, status: data?.status ?? 0)
                }
            } else {
                self.interactorOutput?.authenticationResult(isSuccess: false, status: data?.status ?? 0)
            }
        }
    }
}
