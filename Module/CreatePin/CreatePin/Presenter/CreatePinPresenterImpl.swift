//
//  CreatePinPresenterImpl.swift
//  CreatePin
//
//  Created by MacBook on 26/05/21.
//

import Foundation
import Core

class CreatePinPresenterImpl: CreatePinPresenter {
    func navigateToLogin() {
        self.router.navigateToLogin()
    }
    
    func createPin(pin: String) {
        self.interactor.postCreatePinData(pin: pin)
    }
    
    
    let view: CreatePinView
    let interactor: CreatePinInteractor
    let router: CreatePinRouter
    
    init(view: CreatePinView, interactor: CreatePinInteractor, router: CreatePinRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
}


extension CreatePinPresenterImpl: CreatePinInteractorOutput {
    func authenticationResult(isSuccess: Bool, status: Int) {
        if isSuccess {
            switch status {
            case 200:
                self.view.showSuccess()
            default:
                print("Test")
            }
        } else {
            self.view.showError()
        }
    }
    
}
