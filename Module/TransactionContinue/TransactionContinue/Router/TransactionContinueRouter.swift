//
//  TrancationContinueRouter.swift
//  TransactionContinue
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core
import UIKit

protocol TransactionContinueRouter {
    func navigateToCheckPin(viewController: UIViewController, transferData: TransferEntity)
    func navigateToTransaction(viewController: UIViewController)
}
