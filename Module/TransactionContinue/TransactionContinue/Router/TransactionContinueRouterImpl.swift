//
//  TrancationContinueRouterImpl.swift
//  TransactionContinue
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core
import UIKit

public class TransactionContinueRouterImpl {
    
    public static func navigateToModule(viewController: UIViewController, transferData: TransferEntity){
        let bundle = Bundle(identifier: "com.casestudy.TransactionContinue")
        let vc = TransactionContinueViewController(nibName: String(describing: TransactionContinueViewController.self), bundle: bundle)
        
        
        let router = TransactionContinueRouterImpl()
        let interactor = TransactionContinueInteractorImpl()
        let presenter = TransactionContinuePresenterImpl(view: vc, interactor: interactor, router: router)
      
//        interactor.interactorOutput = presenter

        vc.presenter = presenter
        vc.transferData = transferData
        
        viewController.navigationController?.setNavigationBarHidden(true, animated: false)
        viewController.navigationController?.pushViewController(vc, animated: true)

    }
    
}

extension TransactionContinueRouterImpl: TransactionContinueRouter {
    func navigateToTransaction(viewController: UIViewController) {
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func navigateToCheckPin(viewController: UIViewController, transferData: TransferEntity) {
        AppRouter.shared.navigateToCheckPin(viewController, transferData)
        
    }
}
