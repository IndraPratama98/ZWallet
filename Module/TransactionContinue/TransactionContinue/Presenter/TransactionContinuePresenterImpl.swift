//
//  TransactionContinuePresenterImpl.swift
//  TransactionContinue
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import UIKit
import Core

public class TransactionContinuePresenterImpl: TransactionContinuePresenter {
    let view: TransactionContinueView
    let interactor: TransactionContinueInteractor
    let router: TransactionContinueRouter
    
    init(view: TransactionContinueView, interactor: TransactionContinueInteractor, router: TransactionContinueRouter){
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func navigateToCheckPin(viewController: UIViewController, transferData: TransferEntity) {
        self.router.navigateToCheckPin(viewController: viewController, transferData: transferData)
    }
    
    func backToTransaction(viewController: UIViewController) {
        self.router.navigateToTransaction(viewController: viewController)
    }
}
