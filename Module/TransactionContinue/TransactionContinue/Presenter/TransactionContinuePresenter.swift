//
//  TransactionContinuePresenter.swift
//  TransactionContinue
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core
import UIKit

protocol TransactionContinuePresenter {
    func backToTransaction(viewController: UIViewController)
    func navigateToCheckPin(viewController: UIViewController, transferData: TransferEntity)
}
