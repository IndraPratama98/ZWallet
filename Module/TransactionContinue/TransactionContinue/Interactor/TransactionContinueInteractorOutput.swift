//
//  TrancationContinueInteractorOutput.swift
//  TransactionContinue
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core

protocol TransactionContinueInteractorOutput {
    func loadTransactionContinue(contacts: ProfileEntity)
}
