//
//  TrancationContinueView.swift
//  TransactionContinue
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import UIKit

protocol TransactionContinueView {
    func showTransactionData()
    
}
