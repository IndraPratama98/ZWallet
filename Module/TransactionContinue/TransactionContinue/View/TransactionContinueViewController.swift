//
//  TrancationContinueViewViewController.swift
//  TransactionContinue
//
//  Created by MacBook on 28/05/21.
//

import UIKit
import Core
import Kingfisher

class TransactionContinueViewController: UIViewController, TransactionContinueView {
    
    
    
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var notesLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var contactView: UIView!
    @IBOutlet weak var amountView: UIView!
    @IBOutlet weak var balanceView: UIView!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var notesView: UIView!
    
    
    var presenter: TransactionContinuePresenter?
    var transferData = TransferEntity(receiverData: ProfileEntity(id: 0, name: "", phone: "", image: ""), amount: 0, balance: 0, date: "", notes: "", pin: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showTransactionData()
        backButton.setImage(UIImage(named: "arrow-left", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil), for: .normal)
        
    }
    
    func showTransactionData() {
        contactView.dropShadowEffect()
        amountView.dropShadowEffect()
        balanceView.dropShadowEffect()
        dateView.dropShadowEffect()
        notesView.dropShadowEffect()
        
        let balance: Int? = UserDefaultHelper.shared.get(key: .balanceLeft)
        amountLabel.text = transferData.amount.formatToIdr()
        balanceLabel.text = balance?.formatToIdr()
        dateLabel.text =  transferData.date
        notesLabel.text =  transferData.notes
        nameLabel.text = transferData.receiverData.name
        phoneLabel.text = transferData.receiverData.phone
        let url = URL(string: transferData.receiverData.image)
        self.profileImage.kf.setImage(with: url)
    }
    
    
    @IBAction func continueAction(_ sender: Any) {
        self.presenter?.navigateToCheckPin(viewController: self, transferData: transferData)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.presenter?.backToTransaction(viewController: self)
    }
    
}

