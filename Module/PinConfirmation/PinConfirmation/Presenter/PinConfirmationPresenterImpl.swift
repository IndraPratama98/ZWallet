//
//  PincConfrimationPresenterImpl.swift
//  PinConfirmation
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core
import UIKit

public class PinConfirmationPresenterImpl: PinConfirmationPresenter {
    
    func navigateToTransactionContinue(viewController: UIViewController) {
        self.router.navigateToTransactionContinue(viewController: viewController)
    }
    
    func checkPin(vewController: UIViewController, transferData: TransferEntity) {
        self.interactor.checkPin(transferData: transferData)
        self.transferData = transferData
        self.viewController = vewController
    }
    
    
    
    
    var transferData = TransferEntity(receiverData: ProfileEntity(id: 0, name: "", phone: "", image: ""), amount: 0, balance: 0, date: "", notes: "", pin: "")
    var viewController: UIViewController?
    
    let view: PinConfirmationView
    let interactor: PinConfirmationInteractor
    let router: PinConfirmationRouter
    
    
    init(view: PinConfirmationView, interactor: PinConfirmationInteractor, router: PinConfirmationRouter){
        self.view = view
        self.interactor = interactor
        self.router = router
    }
}

extension PinConfirmationPresenterImpl: PinConfirmationInteractorOutput {
    
    
    func authenticationResult(isSuccess: Bool, status: Int, message: String) {
        if isSuccess {
            self.router.navigateToTransactionSuccess(viewController: viewController!, transferData: transferData, status: true)
        } else {
            self.view.showError()
        }
    }
    
    
}

