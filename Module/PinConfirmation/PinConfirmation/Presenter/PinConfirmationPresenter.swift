//
//  PincConfrimationPresenter.swift
//  PinConfirmation
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core
import UIKit

protocol PinConfirmationPresenter {
    func checkPin(vewController: UIViewController, transferData: TransferEntity)
    func navigateToTransactionContinue(viewController: UIViewController)
    
}
