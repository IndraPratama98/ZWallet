//
//  PinConfirmationRouter.swift
//  PinConfirmation
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core
import UIKit

protocol PinConfirmationRouter {
    func navigateToTransactionSuccess(viewController: UIViewController, transferData: TransferEntity, status: Bool )
    func navigateToTransactionContinue(viewController: UIViewController) 
}
