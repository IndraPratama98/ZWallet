//
//  PinConfirmationRouterImpl.swift
//  PinConfirmation
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core
import UIKit

public class PinConfirmationRouterImpl: PinConfirmationRouter {
    func navigateToTransactionContinue(viewController: UIViewController) {
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func navigateToTransactionSuccess(viewController: UIViewController, transferData: TransferEntity, status: Bool) {
        AppRouter.shared.navigateTransactionResult(viewController, transferData, status)
    }

    public static func navigateToModule(viewController: UIViewController, transferData: TransferEntity){
        let bundle = Bundle(identifier: "com.casestudy.PinConfirmation")
        let vc = PinConfirmationViewController(nibName: String(describing: PinConfirmationViewController.self), bundle: bundle)
        
        
        let router = PinConfirmationRouterImpl()
        let networkManager = AuthNetworkmanagerImpl()
        let interactor = PinConfirmationInteractorImpl(networkManager: networkManager)
        let presenter = PinConfirmationPresenterImpl(view: vc, interactor: interactor, router: router)
      
        interactor.interactorOutput = presenter
        vc.presenter = presenter
        vc.transferData = transferData
        
        viewController.navigationController?.setNavigationBarHidden(true, animated: false)
        viewController.navigationController?.pushViewController(vc, animated: true)
        
    }
    

}
