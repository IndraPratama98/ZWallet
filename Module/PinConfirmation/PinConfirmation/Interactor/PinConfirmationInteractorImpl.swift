//
//  PincConfrimationInteractorImpl.swift
//  PinConfirmation
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import UIKit
import Core

public class PinConfirmationInteractorImpl: PinConfirmationInteractor {
    
    
    var interactorOutput: PinConfirmationInteractorOutput?
    let authNetworkManager: AuthNetworkManager
    
    init(networkManager: AuthNetworkManager) {
        self.authNetworkManager = networkManager
    }
    
    func checkPin(transferData: TransferEntity) {
        self.authNetworkManager.transactionConfirm(receiver: transferData.receiverData.id, amount: transferData.amount, notes: transferData.notes, pin: transferData.pin) { data, error in
            
            if let dataResponse = data {
                if data?.status == 200 {
                    self.interactorOutput?.authenticationResult(isSuccess: true, status: data?.status ?? 0, message: data?.message ?? "")
                } else if data?.status == 404 {
                    self.interactorOutput?.authenticationResult(isSuccess: false, status: data?.status ?? 0, message: data?.message ?? "")
                }
                
            } else {
                self.interactorOutput?.authenticationResult(isSuccess: true, status: data?.status ?? 0, message: data?.message ?? "")
            }
        }
        
        
    }


}
