//
//  PincConfrimationInteractorOutput.swift
//  PinConfirmation
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import UIKit

protocol PinConfirmationInteractorOutput {
    func authenticationResult(isSuccess: Bool, status: Int, message: String)
    
}
