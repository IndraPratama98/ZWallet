//
//  PincConfrimationInteractor.swift
//  PinConfirmation
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core
import UIKit

protocol PinConfirmationInteractor {
    func checkPin(transferData: TransferEntity)
    
}
