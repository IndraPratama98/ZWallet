//
//  PinConfirmationView.swift
//  PinConfirmation
//
//  Created by MacBook on 28/05/21.
//

import Foundation

protocol PinConfirmationView {
    func showError()
}
