//
//  PinConfirmationViewController.swift
//  PinConfirmation
//
//  Created by MacBook on 28/05/21.
//

import UIKit
import Core
import OTPFieldView
import NVActivityIndicatorView

class PinConfirmationViewController: UIViewController, OTPFieldViewDelegate{
    
    
    var otp: String = ""
    
    @IBOutlet weak var otpTextFieldView: OTPFieldView!
    
    func setupOtpView() {
        self.otpTextFieldView.fieldsCount = 6
        self.otpTextFieldView.fieldBorderWidth = 2
        self.otpTextFieldView.defaultBorderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.otpTextFieldView.filledBorderColor = #colorLiteral(red: 0.3882352941, green: 0.4745098039, blue: 0.9568627451, alpha: 1)
        self.otpTextFieldView.cursorColor = UIColor.red
        self.otpTextFieldView.displayType = .roundedCorner
        self.otpTextFieldView.fieldSize = 47
        self.otpTextFieldView.separatorSpace = 10
        self.otpTextFieldView.shouldAllowIntermediateEditing = false
        self.otpTextFieldView.delegate = self
        self.otpTextFieldView.initializeUI()
        self.otpTextFieldView.secureEntry = true
        }
    
    
    
    @IBOutlet weak var backButton: UIButton!
    @IBAction func backToTransactionContinue(_ sender: Any) {
        self.presenter?.navigateToTransactionContinue(viewController: self)
    }
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    var presenter: PinConfirmationPresenter?
    var transferData = TransferEntity(receiverData: ProfileEntity(id: 0, name: "", phone: "", image: ""), amount: 0, balance: 0, date: "", notes: "", pin: "")

    override func viewDidLoad() {
        super.viewDidLoad()
        self.backButton.setImage(UIImage(named: "arrow-left", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil), for: .normal)
        setupOtpView()
        
        
        
        
        
        

    }
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        otp = otpString
    }
    
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
                return false
    }

    @IBAction func transferNowAction(_ sender: Any) {
        
        loadingIndicator.type = .lineScale
        loadingIndicator.color = #colorLiteral(red: 0.3857212365, green: 0.4749737978, blue: 0.9563460946, alpha: 1)
        loadingIndicator.startAnimating()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            self.transferData.pin = self.otp
            self.presenter?.checkPin(vewController: self, transferData: self.transferData)
            self.loadingIndicator.stopAnimating()
        }
        
    
    }
    
}


extension PinConfirmationViewController: PinConfirmationView {
    func showError() {
        let alert = UIAlertController(title: "Peringatan", message: "Pin anda Salah!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
