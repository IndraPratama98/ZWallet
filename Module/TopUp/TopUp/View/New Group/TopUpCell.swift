//
//  TopUpCellTableViewCell.swift
//  TopUp
//
//  Created by MacBook on 31/05/21.
//

import UIKit
import Core


class TopUpCell: UITableViewCell {
    
    @IBOutlet weak var backToHome: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var view1: UILabel!
    @IBOutlet weak var view2: UILabel!
    @IBOutlet weak var view3: UILabel!
    @IBOutlet weak var view4: UILabel!
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var view6: UIView!
    @IBOutlet weak var view7: UIView!
    @IBOutlet weak var view8: UIView!
    
    var delegate: TopUpCellDelegate?
    
    
    public func showData(transferData: ProfileEntity) {
        self.profileImage.image = UIImage(named: "topup", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil)
        self.backToHome.setImage(UIImage(named: "arrow-left", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil), for: .normal)
        
        self.view1.dropShadowEffect()
        self.view2.dropShadowEffect()
        self.view3.dropShadowEffect()
        self.view4.dropShadowEffect()
        self.view5.dropShadowEffect()
        self.view6.dropShadowEffect()
        self.view7.dropShadowEffect()
        self.view8.dropShadowEffect()
        
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    @IBAction func backToHome(_ sender: Any) {
        self.delegate?.navigateToHome()
    }
}
