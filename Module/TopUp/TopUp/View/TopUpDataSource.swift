//
//  TopUpDataSource.swift
//  TopUp
//
//  Created by MacBook on 31/05/21.
//

import Foundation
import UIKit
import Core

class TopUpDataSource: NSObject, UITableViewDataSource {
    
    var viewController: TopUpViewController!
    
    var status: Bool = false
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TopUpCell", for: indexPath)
        return cell
    }
}
