//
//  TopUpViewController.swift
//  TopUp
//
//  Created by MacBook on 31/05/21.
//


import UIKit
import Core

class TopUpViewController: UIViewController, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var presenter: TopUpPresenter?
    var datasource = TopUpDataSource()
    
    func setupTableView() {
        self.datasource.viewController = self
        let bundle = Bundle(identifier: "com.casestudy.TopUp")
        self.tableView.register(UINib(nibName: "TopUpCell", bundle: bundle), forCellReuseIdentifier: "TopUpCell")
        self.tableView.dataSource = self.datasource
        self.tableView.delegate = self
    }
    

    @IBOutlet weak var tableVIew: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }


    

}

extension TopUpViewController: TopUpView {
    func showError() {
        
    }
    
    func showSuccess() {
        
    }
}

extension TopUpViewController: TopUpCellDelegate {
    func navigateToHome() {
        self.presenter?.navigateToHome(viewController: self)
    }
    
    
}


