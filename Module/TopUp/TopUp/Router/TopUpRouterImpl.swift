//
//  TopUpRouterImpl.swift
//  TopUp
//
//  Created by MacBook on 31/05/21.
//

import Foundation
import UIKit

public class TopUpRouterImpl: TopUpRouter  {
    func navigateToHome(viewController: UIViewController) {
        viewController.navigationController?.popToRootViewController(animated: true)
    }
    
    
    
    
    public static func navigateToModule(viewController: UIViewController){
        let bundle = Bundle(identifier: "com.casestudy.TopUp")
        
        let vc = TopUpViewController(nibName: String(describing: TopUpViewController.self), bundle: bundle)
//
//        let router = SetOtpRouterImpl()
//        let networkManager = AuthNetworkmanagerImpl()
//        let interactor = SetOtpInteractorImpl(networkManager: networkManager)
//        let presenter = SetOtpPresenterImpl(view: vc, interactor: interactor, router: router)
//
//        interactor.interactorOutput = presenter
//        vc.presenter = presenter


        
        viewController.navigationController?.setNavigationBarHidden(true, animated: false)
        viewController.navigationController?.pushViewController(vc, animated: true)
        
//        UIApplication.shared.windows.first?.rootViewController = vc
//        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
}
