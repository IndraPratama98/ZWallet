//
//  TopUpInteractor.swift
//  TopUp
//
//  Created by MacBook on 31/05/21.
//

import Foundation

protocol TopUpInteractor {
    func topUP()
}
