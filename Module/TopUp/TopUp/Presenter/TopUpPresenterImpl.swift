//
//  TopUpPresenterImpl.swift
//  TopUp
//
//  Created by MacBook on 31/05/21.
//


import Foundation
import UIKit

public class TopUpPresenterImpl: TopUpPresenter {
    func navigateToHome(viewController: UIViewController) {
        self.router.navigateToHome(viewController: viewController)
    }
    
    
    
    let view: TopUpView
    let interactor: TopUpInteractor
    let router: TopUpRouter
    
    init(view: TopUpView, interactor: TopUpInteractor, router: TopUpRouter){
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    
}

