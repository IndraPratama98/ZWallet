//
//  TopUpPresenter.swift
//  TopUp
//
//  Created by MacBook on 31/05/21.
//

import Foundation
import UIKit

protocol TopUpPresenter {
    func navigateToHome(viewController: UIViewController)
}
