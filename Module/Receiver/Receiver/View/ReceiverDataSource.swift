//
//  ReceiverDataSOurce.swift
//  Receiver
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import UIKit
import Core


class ReceiverDataSource: NSObject, UITableViewDataSource {
    
    var viewController: ReceiverViewController!
    var profile: [ProfileEntity] = []
    var profileFilter: [ProfileEntity] = []
    
//    var historyHeader = ["Quick Access", "All Contacts"]
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return profileFilter.count
//        if section == 0 {
//            return profileFilter.count
//        } else {
//            return profileFilter.count
//        }
    }
    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 2
//    }
    
   
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath) as! ContactCell
        cell.showData(contact: self.profileFilter[indexPath.row])
        cell.delegate = self.viewController
        return cell
    }
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return historyHeader[section]
//    }
//

    
}

