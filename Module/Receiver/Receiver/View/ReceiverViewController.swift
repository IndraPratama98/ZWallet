//
//  ReceiverViewController.swift
//  Receiver
//
//  Created by MacBook on 27/05/21.
//

import UIKit
import Core
import NVActivityIndicatorView

class ReceiverViewController: UIViewController, UISearchBarDelegate {
    var dataSource = ReceiverDataSource()
    var presenter: ReceiverPresenter?
    
    @IBOutlet weak var countText: UILabel!
    @IBOutlet weak var backToHomeButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadingIndicator.type = .lineScale
        loadingIndicator.color = #colorLiteral(red: 0.3857212365, green: 0.4749737978, blue: 0.9563460946, alpha: 1)
        loadingIndicator.startAnimating()
    
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            self.setupTableView()
            self.presenter?.loadContacts()
            self.loadingIndicator.stopAnimating()
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    
    @IBAction func backToHomeAction(_ sender: Any) {
        self.presenter?.navigateToHome()
    }
    
    func setupTableView() {
        self.dataSource.viewController = self
        self.tableView.register(UINib(nibName: "ContactCell", bundle: Bundle(identifier: "com.casestudy.Receiver")), forCellReuseIdentifier: "ContactCell")
        self.tableView.dataSource = self.dataSource
        self.searchBar.delegate = self
        self.backToHomeButton.setImage(UIImage(named: "arrow-left", in: Bundle(identifier: "com.casestudy.Core"), compatibleWith: nil), for: .normal)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        dataSource.profileFilter = dataSource.profile
        if searchText.isEmpty == false {
            dataSource.profileFilter = dataSource.profileFilter.filter({$0.name.contains(searchText)})
            self.countText.text = "\(dataSource.profileFilter.count) Contact Found"
        } else {
            self.countText.text = "\(dataSource.profile.count) Contact Found"
        }
        tableView.reloadData()
    }
    

}

extension ReceiverViewController: ReceiverView {
    func showContactData(contacts: [ProfileEntity]) {
        self.dataSource.profile = contacts
        self.dataSource.profileFilter = contacts
        self.tableView.reloadData()
        self.countText.text = "\(dataSource.profile.count) Contact Found"
    }
}

extension ReceiverViewController: ContactCellDelegate {
    func navigateToTransaction(receiverData: ProfileEntity) {
        self.presenter?.navigateToTransaction(recceiverData: receiverData, viewController: self)
    }
    
    func navigateToTransaction() {
        self.presenter?.navigateToHome()
    }
}






