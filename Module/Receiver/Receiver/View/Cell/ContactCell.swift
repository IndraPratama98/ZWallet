//
//  ContactCell.swift
//  Core
//
//  Created by MacBook on 27/05/21.
//

import UIKit
import Kingfisher
import Core


public class ContactCell: UITableViewCell {

    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var cellView: UIView!
    
    var url: String = ""
    var id: Int = 0
    var delegate: ContactCellDelegate?
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        view.addGestureRecognizer(tap)
        cellView.dropShadowEffect()
    }

    public func showData(contact: ProfileEntity) {
        self.nameLabel.text = contact.name
        self.phoneLabel.text = contact.phone
        self.id = contact.id
        self.url = contact.image
        self.profileImage.kf.setImage(with: URL(string: contact.image))
    }
    
    

    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.delegate?.navigateToTransaction(receiverData: ProfileEntity(id: self.id, name: self.nameLabel.text ?? "", phone: self.phoneLabel.text ?? "", image: self.url))
    }
    
}
