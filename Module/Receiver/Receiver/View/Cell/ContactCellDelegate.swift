//
//  ContactCellDelegate.swift
//  Receiver
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import UIKit
import Core

protocol ContactCellDelegate {
    func navigateToTransaction(receiverData: ProfileEntity)
}
