//
//  RecevierView.swift
//  Receiver
//
//  Created by MacBook on 26/05/21.
//

import Foundation
import Core

protocol ReceiverView {
    func showContactData(contacts: [ProfileEntity])
}
