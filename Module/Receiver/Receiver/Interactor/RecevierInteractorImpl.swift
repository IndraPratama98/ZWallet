//
//  RecevierInteractorImpl.swift
//  Receiver
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Core

public class ReceiverInteractorImpl: ReceiverInteractor {
    
    var interactorOutput: ReceiverInteractorOutput?
    let contactNetworkManager: ContactNetworkManager
    
    init(contactNetworkManager: ContactNetworkManager){
        self.contactNetworkManager = contactNetworkManager
    }
    
    
    func getAllContacts() {
        self.contactNetworkManager.getAllContacts { (data, error) in
            var contacts: [ProfileEntity] = []
            
            data?.forEach({ (contactData) in
                contacts.append(ProfileEntity(id: contactData.id, name: contactData.name, phone: contactData.phone, image: "\(AppConstant.baseUrl)\(contactData.image)"))
                self.interactorOutput?.loadContacts(contacts: contacts)
                
            })
        }
    }
    
    
    
}
