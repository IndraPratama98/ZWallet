//
//  ReceiverPresenter.swift
//  Receiver
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import UIKit
import Core

protocol ReceiverPresenter {
    func loadContacts()
    func navigateToHome()
    func navigateToTransaction(recceiverData: ProfileEntity, viewController: UIViewController)
}
