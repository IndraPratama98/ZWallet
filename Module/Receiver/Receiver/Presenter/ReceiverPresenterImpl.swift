//
//  ReceiverPresenterImpl.swift
//  Receiver
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Core
import UIKit

class ReceiverPresenterImpl: ReceiverPresenter {
    func navigateToTransaction(recceiverData: ProfileEntity, viewController: UIViewController) {
        self.router.navigateToTransaction(viewController: viewController, receiverData: recceiverData)
    }
    
    func navigateToHome() {
        self.router.navigateToHome()
    }
    
    func loadContacts() {
        self.interactor.getAllContacts()
    }
    

    let view: ReceiverView
    let interactor: ReceiverInteractor
    let router: ReceiverRouter
    
    init(view: ReceiverView, interactor: ReceiverInteractor, router: ReceiverRouter){
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
}


extension ReceiverPresenterImpl: ReceiverInteractorOutput {
    func loadContacts(contacts: [ProfileEntity]) {
        self.view.showContactData(contacts: contacts)
    }
}
