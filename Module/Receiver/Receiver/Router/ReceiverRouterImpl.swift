//
//  ReceiverRouterImpl.swift
//  Receiver
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Core
import UIKit

public class ReceiverRouterImpl: ReceiverRouter {
    func navigateToTransaction(viewController: UIViewController, receiverData: ProfileEntity) {
        AppRouter.shared.navigateToTransaction(viewController, receiverData)
    }
    
    public static func navigateToModule(viewController: UIViewController){
        let bundle = Bundle(identifier: "com.casestudy.Receiver")
        let vc = ReceiverViewController(nibName: String(describing: ReceiverViewController.self), bundle: bundle)
        
        let contactNetworkManager = ContactNetworkManagerImpl()
        
        let router = ReceiverRouterImpl()
        let interactor = ReceiverInteractorImpl(contactNetworkManager: contactNetworkManager)
        let presenter = ReceiverPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter
        vc.presenter = presenter
        
        viewController.navigationController?.setNavigationBarHidden(true, animated: true)
        viewController.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func navigateToHome() {
        AppRouter.shared.navigateToHome()
    }
}
